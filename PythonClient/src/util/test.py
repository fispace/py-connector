'''
Created on 09/04/2015

@author: Jordi Hernandez 
Version: 0.16.0
'''
import PythonConnector
from beans import CapabilityType, Capability, BusinessProcessTemplate, BusinessProcess, ReceiveShipmentStatusRequestMessage, Role, BusinessProcessTemplateRole, BusinessProcessRole 

token="eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIzMGU5ZDc2Mi0xN2I1LTQ1YmItYmIzMi1jZGExZmFmYjA2NmYiLCJleHAiOjE0MzYxNzAwNjcsIm5iZiI6MCwiaWF0IjoxNDM2MTcwMDA3LCJpc3MiOiJmaXNwYWNlIiwiYXVkIjoiY29ubmVjdG9yIiwic3ViIjoiNTFhNDVmODQtMmFjMi00OWYwLTk1MTctODNkYjA2YWFlNDc0IiwiYXpwIjoiY29ubmVjdG9yIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiUzI4NzUyNiIsInNlc3Npb25fc3RhdGUiOiI2MjgzM2Y2ZC01YmM3LTQzZDItYWNlNi05MjAwYzMxYmRhNjMiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovL2xvY2FsaG9zdCJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiYXBwX2RldmVsb3BlciIsImJ1c2luZXNzX2FyY2hpdGVjdCJdfSwicmVzb3VyY2VfYWNjZXNzIjp7fX0.BRi6EJ-NqLTDNXpEqbMv2b4ub85MrdT29MOcHByyNCFLhZ7bZmrecoeWTWq3_mAxvkHYVQvu-bnUZcBDJXWR-EFuOOEUKxJJOxHcJAi_XT0MWB69_wI2gHNSkiAZK6b2KOujggfUkJ6jwIEQb82S9Q8_1LD8aqONj1LM0GlGqEw"


"""
CapabilityTypes
"""
"""
capabilityType = CapabilityType()
capabilityType.setNameCap("cap100")
capabilityType.setRequestMessageType("request100")
capabilityType.setResponseMessageType("response100")
capabilityType.setContextPath("context100")
capabilityType.setSchemaLocation("schema100")

response=PythonConnector.updateCapabilityType(token, capabilityType)
print(response)
"""

"""
response=PythonConnector.removeCapabilityType(token, "41")
print(response)
"""

"""
response=PythonConnector.getCapabilityTypes(token)
print(response)
"""    

"""
response=PythonConnector.getCapabilityType(token, "40")
print(response)
"""

"""
Capabilities
"""

"""
capability = Capability()
capability.setDescription("c1619")
capability.setUri("urn:test")
capability.setCteId("40")

response=PythonConnector.updateCapability(token, capability)
print(response)
"""

"""
response=PythonConnector.removeCapability(token, "11")
print(response)
"""

"""
response=PythonConnector.getCapabilities(token)
print(response)
"""

"""
response=PythonConnector.getCapability(token, "2")
print(response)
"""

"""
BusinessProcessTemplates
"""

"""
businessProcessTemplate = BusinessProcessTemplate()
businessProcessTemplate.setName("bpt1624")
listCT = ['40']
businessProcessTemplate.setListCT(listCT)

response=PythonConnector.updateBusinessProcessTemplate(token, businessProcessTemplate)
print(response)
"""

"""
businessProcessTemplateRole = BusinessProcessTemplateRole()
businessProcessTemplateRole.setName("bpt1624")
role = Role()
role.setIdRole("1")
listCT = ['54', '55']
role.setListId(listCT)
role1 = Role()
role1.setIdRole("2")
role1.setListId(listCT)

listRole = [role, role1]
businessProcessTemplateRole.setRole(listRole)

response=PythonConnector.updateBusinessProcessTemplateRole(token, businessProcessTemplateRole)
print(response)
"""


"""
response=PythonConnector.removeBusinessProcessTemplate(token, "5")
print(response)
"""

"""
response=PythonConnector.getBusinessProcessTemplates(token)
print(response)
"""

"""
response=PythonConnector.getBusinessProcessTemplate(token, "1")
print(response)
"""

"""
BusinessProcesses
"""

"""
businessProcess = BusinessProcess()
businessProcess.setName("bp1149")
businessProcess.setBptId("5")
listC = ['11']
businessProcess.setListC(listC)

response=PythonConnector.updateBusinessProcess(token, businessProcess)
print(response)
"""


businessProcessRole = BusinessProcessRole()
businessProcessRole.setName("bp1149")
businessProcessRole.setBptId("29")
role = Role()
role.setIdRole("1")
listC = ['49', '50']
role.setListId(listC)
role1 = Role()
role1.setIdRole("2")
role1.setListId(listC)
listRole = [role, role1]
businessProcessRole.setRole(listRole)

response=PythonConnector.updateBusinessProcessRole(token, businessProcessRole)
print(response)


"""
response=PythonConnector.removeBusinessProcess(token, "5")
print(response)
"""

"""
response=PythonConnector.getBusinessProcesses(token)
print(response)
"""

"""
response=PythonConnector.getBusinessProcess(token, "2")
print(response)
"""

"""
ReceiveShipmentStatusRequestMessage
"""
"""
receiveShipmentStatusRequestMessage = ReceiveShipmentStatusRequestMessage()
receiveShipmentStatusRequestMessage.setBusinessProcessId("1")
receiveShipmentStatusRequestMessage.setStatus("Announced")
receiveShipmentStatusRequestMessage.setReason("1")
receiveShipmentStatusRequestMessage.setMessageId("1")
receiveShipmentStatusRequestMessage.setSenderId("1")
receiveShipmentStatusRequestMessage.setReceiverId("1")
receiveShipmentStatusRequestMessage.setSenderAppType("1")
receiveShipmentStatusRequestMessage.setReceiverAppType("1")
receiveShipmentStatusRequestMessage.setShipmentId("1")
receiveShipmentStatusRequestMessage.setShipmentDataLink("1")

name = "receive_shipment_status"
response=PythonConnector.postRequestMessage(token, name, receiveShipmentStatusRequestMessage)
print(response)
"""