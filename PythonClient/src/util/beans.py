'''
Created on 09/04/2015

@author: Jordi Hernandez 
Version: 0.16.0
'''

"""
CapabilityType class
"""
class CapabilityType:
    nameCap = ""
    requestMessageType = ""
    responseMessageType = ""
    schemaLocation = ""
    contextPath = ""


    def __init__(self):
        '''
        Constructor
        '''
        
    def getNameCap(self):
        return self.nameCap

    def setNameCap(self, name):
        self.nameCap = name
        
    def getRequestMessageType(self):
        return self.requestMessageType

    def setRequestMessageType(self, request):
        self.requestMessageType = request
        
    def getResponseMessageType(self):
        return self.responseMessageType

    def setResponseMessageType(self, response):
        self.responseMessageType = response
        
    def getSchemaLocation(self):
        return self.schemaLocation

    def setSchemaLocation(self, schema):
        self.schemaLocation = schema               
        
    def getContextPath(self):
        return self.contextPath

    def setContextPath(self, context):
        self.contextPath = context                   
        
"""
Capability class
"""
class Capability:
    description = ""
    uri = ""
    cteId = ""
    
    def __init__(self):
        '''
        Constructor
        '''
        
    def getDescription(self):
        return self.description

    def setDescription(self, desc):
        self.description = desc
        
    def getUri(self):
        return self.uri

    def setUri(self, url):
        self.uri = url
        
    def getCteId(self):
        return self.cteId

    def setCteId(self, cte):
        self.cteId = cte
        
"""
BusinessProcessTemplate class
"""        
class BusinessProcessTemplate:
    name = ""
    listCT = []
        
    def __init__(self):
        '''
        Constructor
        '''
        
    def getName(self):
        return self.name

    def setName(self, n):
        self.name = n
        
    def getListCT(self):
        return self.listCT

    def setListCT(self, l):
        self.listCT = l
        
"""
BusinessProcess class
"""    
class BusinessProcess:
    name = ""
    bptId = ""
    listC = []
        
    def __init__(self):
        '''
        Constructor
        '''
        
    def getName(self):
        return self.name

    def setName(self, n):
        self.name = n

    def getBptId(self):
        return self.bptId

    def setBptId(self, b):
        self.bptId = b
          
    def getListC(self):
        return self.listC

    def setListC(self, l):
        self.listC = l
        
                                      
"""
ReceiveShipmentStatusRequestMessage: class
"""    
class ReceiveShipmentStatusRequestMessage:
    businessProcessId=""
    status=""
    reason=""
    messageId=""
    senderId=""
    receiverId=""
    senderAppType=""
    receiverAppType=""
    shipmentId=""
    shipmentDataLink=""
        
    def __init__(self):
        '''
        Constructor
        '''
        
    def getBusinessProcessId(self):
        return self.businessProcessId

    def setBusinessProcessId(self, value):
        self.businessProcessId = value

    def getStatus(self):
        return self.status

    def setStatus(self, value):
        self.status = value
        
    def getReason(self):
        return self.reason

    def setReason(self, value):
        self.reason = value    
        
    def getMessageId(self):
        return self.messageId

    def setMessageId(self, value):
        self.messageId = value    
        
    def getSenderId(self):
        return self.senderId

    def setSenderId(self, value):
        self.senderId = value 
        
    def getReceiverId(self):
        return self.receiverId

    def setReceiverId(self, value):
        self.receiverId = value
        
    def getSenderAppType(self):
        return self.senderAppType

    def setSenderAppType(self, value):
        self.senderAppType = value          
        
    def getReceiverAppType(self):
        return self.receiverAppType

    def setReceiverAppType(self, value):
        self.receiverAppType = value              
        
    def getShipmentId(self):
        return self.shipmentId

    def setShipmentId(self, value):
        self.shipmentId = value
        
    def getShipmentDataLink(self):
        return self.shipmentDataLink

    def setShipmentDataLink(self, value):
        self.shipmentDataLink = value

"""
Role class
"""        
class Role:
    idRole = ""
    listId = []
        
    def __init__(self):
        '''
        Constructor
        '''
        
    def getIdRole(self):
        return self.idRole

    def setIdRole(self, n):
        self.idRole = n
        
    def getListId(self):
        return self.listId

    def setListId(self, l):
        self.listId = l
                      
    """
BusinessProcessTemplate class
"""        
class BusinessProcessTemplateRole:
    name = ""
    Role = []
        
    def __init__(self):
        '''
        Constructor
        '''
        
    def getName(self):
        return self.name

    def setName(self, n):
        self.name = n
        
    def getRole(self):
        return self.Role

    def setRole(self, l):
        self.Role = l
        
"""
BusinessProcess class
"""    
class BusinessProcessRole:
    name = ""
    bptId = ""
    Role = []
        
    def __init__(self):
        '''
        Constructor
        '''
        
    def getName(self):
        return self.name

    def setName(self, n):
        self.name = n

    def getBptId(self):
        return self.bptId

    def setBptId(self, b):
        self.bptId = b
          
    def getRole(self):
        return self.Role

    def setRole(self, l):
        self.Role = l                                    