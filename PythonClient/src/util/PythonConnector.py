'''
Created on 09/04/2015

@author: Jordi Hernandez 
Version: 0.16.0
'''

import requests
from util.constants import ConstantsName
from xml.dom.minidom import parseString
import xml.etree.ElementTree as ET

'''
GET Methods
'''
def getCapabilityTypes(token):
    resourceuri = "capability-types";
    finaluri = ConstantsName.BASE_URI + resourceuri

    response=getMethod(finaluri, "application/xml", token)
    
    return response


def getCapabilityType(token, value):
        
    if value:
        resourceuri = "capability-types";
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value
    
        response=getMethod(finaluri, "application/xml", token)
    
    return response


def getCapabilities(token):
    resourceuri = "capabilities"
    finaluri = ConstantsName.BASE_URI + resourceuri

    response=getMethod(finaluri, "application/xml", token)

    return response


def getCapability(token, value):
    if value:
        resourceuri = "capabilities"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

        response=getMethod(finaluri, "application/xml", token)
    
    return response


def getBusinessProcesses(token):
    resourceuri = "business-processes"
    finaluri = ConstantsName.BASE_URI + resourceuri

    response=getMethod(finaluri, "application/xml", token)

    return response


def getBusinessProcess(token, value):
    if value:
        resourceuri = "business-processes"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

        response=getMethod(finaluri, "application/xml", token)
    
    return response


def getBusinessProcessTemplates(token):
    resourceuri = "business-process-templates"
    finaluri = ConstantsName.BASE_URI + resourceuri

    response=getMethod(finaluri, "application/xml", token)

    return response


def getBusinessProcessTemplate(token, value):
    if value:
        resourceuri = "business-process-templates"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

        response=getMethod(finaluri, "application/xml", token)
    
    return response


# Generic GET Method
def getMethod(url, content, token):
    response=""
    
    try:
        headers={
        'Content-Type':content,
        'Accept':content,
        'Authorization':'Bearer ' + token
        }
        
        r = requests.get(url, headers=headers, verify=False)
                
        response = parseString(r.text)
        response = response.toprettyxml(indent="\t")
                
    except Exception as e:
        response="Error to access to " + url
    
    
    return response


'''
PUT Methods
'''
def updateCapability(token, capability):
    resourceuri = "capabilities"
    finaluri = ConstantsName.BASE_URI + resourceuri
        
    description=capability.getDescription()
    uri=capability.getUri()
    cteId=capability.getCteId()
        
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + '<Capability xmlns="http://www.limetri.eu/schemas/ygg">' + '<description>' + description + '</description>' + '<uri>' + uri + '</uri>' + '<cte_id>' + cteId + '</cte_id>' + '</Capability>' 
    
    response=putMethod(finaluri, body, token, "application/xml")
        
    return response  

def updateCapabilityType(token, capabilityType):
    resourceuri = "capability-types"
    finaluri = ConstantsName.BASE_URI + resourceuri

    name=capabilityType.getNameCap()
    request=capabilityType.getRequestMessageType()
    response=capabilityType.getResponseMessageType()
    context=capabilityType.getContextPath()
    schema=capabilityType.getSchemaLocation()
    
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + '<capabilityType xmlns="http://www.limetri.eu/schemas/ygg">' + '<name>' + name + '</name>' + '<requestMessageType>' + request + '</requestMessageType>' + '<responseMessageType>' + response + '</responseMessageType>' + '<schemaLocation>' + schema + '</schemaLocation>' + '<contextPath>' + context + '</contextPath>' + '</capabilityType>'
    
    response=putMethod(finaluri, body, token, "application/xml")

    return response

def updateBusinessProcessTemplate(token, businessProcessTemplate):
    resourceuri = "business-process-templates"
    finaluri = ConstantsName.BASE_URI + resourceuri
    
    name = businessProcessTemplate.getName()
    listCT = businessProcessTemplate.getListCT()
        
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
    body = body + '<BusinessProcessTemplate xmlns="http://www.limetri.eu/schemas/ygg">'
    body = body + '<name>' + name + '</name>' 
    for i in listCT:
        body = body + '<capabilityTypes>'
        body = body + '<id>' + i + '</id>'
        body = body + '<name>name</name>' 
        body = body + '<requestMessageType>request</requestMessageType>' 
        body = body + '<responseMessageType>response</responseMessageType>' 
        body = body + '<schemaLocation>schema</schemaLocation>' 
        body = body + '<contextPath>eu.fispace.api.lg</contextPath>'
        body = body + '</capabilityTypes>'
            
    body = body + '</BusinessProcessTemplate>'
            
    response=putMethod(finaluri, body, token, "application/xml")

    return response


def updateBusinessProcess(token, businessProcess):
    resourceuri = "business-processes"
    finaluri = ConstantsName.BASE_URI + resourceuri
    capability=''
    
    name=businessProcess.getName()
    bptId=businessProcess.getBptId()
    listC = businessProcess.getListC()
        
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
    body = body + '<BusinessProcess xmlns="http://www.limetri.eu/schemas/ygg">'
    body = body + '<name>' + name + '</name>' + '<bpt_id>' + bptId + '</bpt_id>'
            
    for i in listC:
        capability=''
        xmlcap=''
        #find capability
        capability = getCapability(token, i)
                
        try:
            xmlcap = ET.fromstring(capability)
                       
            for child in xmlcap:
                nametag=child.tag
                        
                if nametag.find("cte_id")!=-1:
                    body = body + '<capabilities>' 
                    body = body + '<id>' + i + '</id>'
                    body = body + '<description>bpdesc</description>'
                    body = body + '<uri>urn:uri</uri>'
                    body = body + '<cte_id>' + child.text + '</cte_id>'
                    body = body + '</capabilities>'
            
        except Exception as e:
            body = body + ''
        
    body = body + '</BusinessProcess>'
    
    response=putMethod(finaluri, body, token, "application/xml")
     
    return response

def updateBusinessProcessRole(token, businessProcessRole):
    resourceuri = "business-processes"
    finaluri = ConstantsName.BASE_URI + resourceuri
    capability=''
    
    name=businessProcessRole.getName()
    bptId=businessProcessRole.getBptId()
    listRole = businessProcessRole.getRole()
        
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
    body = body + '<BusinessProcess xmlns="http://www.limetri.eu/schemas/ygg">'
    body = body + '<name>' + name + '</name>' + '<bpt_id>' + bptId + '</bpt_id>'
     
    for r in listRole:      
        body = body + '<role><id>' + r.getIdRole() + '</id>'
        listC = r.getListId() 
        for i in listC:
            capability=''
            xmlcap=''
            #find capability
            capability = getCapability(token, i)
                    
            try:
                xmlcap = ET.fromstring(capability)
                           
                for child in xmlcap:
                    nametag=child.tag
                            
                    if nametag.find("cte_id")!=-1:
                        body = body + '<capability>' 
                        body = body + '<id>' + i + '</id>'
                        body = body + '<description>bpdesc</description>'
                        body = body + '<uri>urn:uri</uri>'
                        body = body + '<cte_id>' + child.text + '</cte_id>'
                        body = body + '</capability>'
                
            except Exception as e:
                body = body + ''
        
        body = body + '</role>'
    body = body + '</BusinessProcess>'
    
    response=putMethod(finaluri, body, token, "application/xml")
     
    return response
    
def updateBusinessProcessTemplateRole(token, businessProcessTemplateRole):
    resourceuri = "business-process-templates"
    finaluri = ConstantsName.BASE_URI + resourceuri
    
    name = businessProcessTemplateRole.getName()
    listRole = businessProcessTemplateRole.getRole()
        
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
    body = body + '<BusinessProcessTemplate xmlns="http://www.limetri.eu/schemas/ygg">'
    body = body + '<name>' + name + '</name>' 
    
    for r in listRole:
        body = body + '<role><id>' + r.getIdRole() + '</id>'
        listCT = r.getListId()
        for i in listCT:
            body = body + '<capabilityType>'
            body = body + '<id>' + i + '</id>'
            body = body + '<name>name</name>' 
            body = body + '<requestMessageType>request</requestMessageType>' 
            body = body + '<responseMessageType>response</responseMessageType>' 
            body = body + '<schemaLocation>schema</schemaLocation>' 
            body = body + '<contextPath>eu.fispace.api.lg</contextPath>'
            body = body + '</capabilityType>'
        body = body + '</role>'   
            
    body = body + '</BusinessProcessTemplate>'
            
    response=putMethod(finaluri, body, token, "application/xml")

    return response    

# Generic PUT method
def putMethod(url, body, token, content):
        
    try:
        headers={
        'Content-Type':content,
        'Accept':content,
        'Authorization':'Bearer ' + token
        }
        
        response = requests.put(url, headers=headers, data=body, verify=False)
        
    except Exception as e:
        response=e.strerror
    return response

"""
DELETE Methods
"""
def removeCapabilityType(token, value):
    if value:
        resourceuri = "capability-types"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

    response=delMethod(finaluri, "application/xml", token)
    
    return response


def removeCapability(token, value):
    if value:
        resourceuri = "capabilities"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

    response=delMethod(finaluri, "application/xml", token)
    
    return response


def removeBusinessProcess(token, value):
    if value:
        resourceuri = "business-processes"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

    response=delMethod(finaluri, "application/xml", token)
    
    return response


def removeBusinessProcessTemplate(token, value):
    if value:
        resourceuri = "business-process-templates"
        finaluri = ConstantsName.BASE_URI + resourceuri + "/" + value

        response=delMethod(finaluri, "application/xml", token)
    
    return response

"""
Generic DELETE method
"""
def delMethod(url, content, token):
    try:
        headers={'Authorization':'Bearer ' + token}
        
        response = requests.delete(url, headers=headers, verify=False)
        
    except Exception as e:
        response=e.strerror
    return response

"""
POST Methods
"""
def postRequestMessage(token, name, receiveShipmentStatusRequestMessage):
    resourceuri = "capabilities/" + name + "/use"
    finaluri = ConstantsName.BASE_URI_USE + resourceuri
        
    businessProcessId=receiveShipmentStatusRequestMessage.getBusinessProcessId()
    status=receiveShipmentStatusRequestMessage.getStatus()
    reason=receiveShipmentStatusRequestMessage.getReason()
    messageId=receiveShipmentStatusRequestMessage.getMessageId()
    senderId=receiveShipmentStatusRequestMessage.getSenderId()
    receiverId=receiveShipmentStatusRequestMessage.getReceiverId()
    senderAppType=receiveShipmentStatusRequestMessage.getSenderAppType()
    receiverAppType=receiveShipmentStatusRequestMessage.getReceiverAppType()
    shipmentId=receiveShipmentStatusRequestMessage.getShipmentId()
    shipmentDataLink=receiveShipmentStatusRequestMessage.getShipmentDataLink()
    
    body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' 
    body = body + '<ns3:ReceiveShipmentStatusRequestMessage xmlns:ns2="http://www.limetri.eu/schemas/ygg" xmlns:ns3="http://www.fispace.eu/domain/lg">'
    body = body + '<ns2:businessProcessId>' + businessProcessId + '</ns2:businessProcessId>'
    body = body + '<messageId>' + messageId + '</messageId>'
    body = body + '<senderId>' + senderId + '</senderId>'
    body = body + '<receiverId>' + receiverId + '</receiverId>'
    body = body + '<senderAppType xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'
    body = body + '<receiverAppType xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'
    body = body + '<shipmentId>' + shipmentId + '</shipmentId>'
    body = body + '<shipmentDataLink>' + shipmentDataLink + '</shipmentDataLink>'
    body = body + '<status>' + status + '</status>'
    body = body + '</ns3:ReceiveShipmentStatusRequestMessage>'
    
    response=postMethod(finaluri, body, token, "application/xml")

    return response

    
# Generic POST method
def postMethod(url, body, token, content):
        
    try:
        headers={
        'Content-Type':content,
        'Accept':content,
        'Authorization':'Bearer ' + token
        }
        
        response = requests.post(url, data=body, headers=headers, verify=False)
        
    except Exception as e:
        response=e.strerror
    return response
